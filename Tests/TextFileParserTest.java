package lab1v2.Tests;


import lab1v2.TextFileParser;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;


/**
 * Created by evgenijpopov on 13.06.17.
 */
class TextFileParserTest {
    String[] positivePatterns = {"фыв", "123", "123йцук", ".,;", "1фябю.!"};
    String[] negativePatterns = {"asd", "11a23", "123йцук$", "#.,;", "", null};

    private TextFileParser textFileParser;

    @BeforeEach
    void setUp() {
        textFileParser = new TextFileParser("");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void processWords() {
        Field fld;
        Method method = null;
        ConcurrentHashMap<String, Boolean> wordsMap = null;
        try {
            fld = textFileParser.getClass().getDeclaredField("wordsMap");
            fld.setAccessible(true);
            wordsMap = (ConcurrentHashMap<String, Boolean>) fld.get(textFileParser);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            fail("Not find field \"wordsMap\"!");
        }

        try {
            method = textFileParser.getClass().getDeclaredMethod("processWords", positivePatterns.getClass());
            method.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            fail("No method processWords()");
        }


        try {
            assertTrue((Boolean) method.invoke(textFileParser, new Object[]{positivePatterns}));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            fail("No method processWords()");
        }
        for (String key : positivePatterns) {
            assertTrue(wordsMap.get(key));
        }

        try {
            assertFalse((Boolean) method.invoke(textFileParser, new Object[]{negativePatterns}));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            fail("No method processWords()");
        }
        for (String key : negativePatterns) {
            if (key == null) continue;
            assertFalse(wordsMap.getOrDefault(key, false));
        }
    }

    @Test
    void LegalChars() {
        Method method = null;
        try {
            method = textFileParser.getClass().getDeclaredMethod("isLegalChars", String.class);
            method.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            fail("No method isLegalChars()");
        }

        for (String pt : positivePatterns) {
            try {
                assertTrue((Boolean) method.invoke(textFileParser, pt));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                fail("Error while invoke method");
            }
        }

        for (String pt : negativePatterns) {
            try {
                assertFalse((Boolean) method.invoke(textFileParser, pt));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                fail(String.format("Error while invoke method with param <%s>", pt));
            }
        }
    }
}