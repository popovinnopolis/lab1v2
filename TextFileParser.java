package lab1v2;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static lab1v2.Main.myDEBUG;


/**
 * Created by evgenijpopov on 09.06.17.
 */
public class TextFileParser extends Thread {
    private final String filename;
    private Pattern legalCharsPattern = Pattern.compile("[а-яА-Я0-9!?,.;:\"\\-\\s()]+");

    private static volatile boolean isStop = false;
    private static ConcurrentHashMap<String, Boolean> wordsMap = new ConcurrentHashMap<>();

    public TextFileParser(String name) {
        this(name, "");
    }

    public TextFileParser(String name, String filename) {
        super(name);
        this.filename = filename;
    }

    @Override
    public void run() {
        if (filename == null || filename == "") {
            isStop = true;
            return;
        }
        if (myDEBUG) System.out.printf("Start parsing file: %s.\n", filename);
        Path path = FileSystems.getDefault().getPath(filename);
        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            String line = null;
            while ((line = reader.readLine()) != null && !isStop) {
                processWords(line.split(" "));
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
            isStop = true;
        }
        if (isStop) {
            System.out.println("Parsing break.");
            return;
        }
        if (myDEBUG) System.out.printf("File: %s successfully parsed.\n", filename);
    }

    private boolean processWords(String[] words) {
        for (String word : words) {
            if (word.length() == 0) continue;

            if (!isLegalChars(word)) {
                System.out.printf("Illegal word: '%s' in file '%s'.\n", word, filename);
                isStop = true;
                return false;
            }

            if (wordsMap.putIfAbsent(word, true) != null) {
                System.out.printf("Duplicate word: '%s' in file '%s'.\n", word, filename);
                isStop = true;
                return false;
            }
        }
        return true;
    }

    private boolean isLegalChars(String testString) {
        if (testString == null) return false;
        Matcher m = legalCharsPattern.matcher(testString);
        return m.matches();
    }

    public static void showStatistic(){
        System.out.printf("\n\nStatistic:\n\tWords: %s \n", wordsMap.size());
    }
}
