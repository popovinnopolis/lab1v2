package lab1v2;

import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.*;

/**
 * Created by evgenijpopov on 09.06.17.
 */
public class Main {
    static final int THREADPOOLSIZE = 5;
    static final boolean myDEBUG = true;
    private static final String fileExtension = myDEBUG ? ".debug" : ".txt";

    private static final String pathForInputFiles = "./data/labz1";

    public static void main(String[] args) {
        FilenameFilter fileNameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (name.lastIndexOf('.') > 0) {

                    int lastIndex = name.lastIndexOf('.');
                    String str = name.substring(lastIndex);
                    if (str.equals(fileExtension)) {
                        return true;
                    }
                }
                return false;
            }
        };

        File[] files = new File(pathForInputFiles).listFiles(fileNameFilter);
        if (files == null) {
            System.out.println("Error. No input files.");
            return;
        }

//        ExecutorService service = Executors.newFixedThreadPool(THREADPOOLSIZE);
        ThreadPoolExecutor service = new ThreadPoolExecutor(5, THREADPOOLSIZE, 1,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(100));

        for (File fl : files) {
            service.execute(new TextFileParser(fl.getName(), fl.toString()));
        }
        service.shutdown();
        while (!service.isTerminated()) {
        }
        TextFileParser.showStatistic();
    }
}
